package com.hubster.dao;

public interface QueryConstants {

	// For Todos

	public String todo_query = "SELECT chid,challenge,chmeetdate,cheKP,chstatusdate,priority,followup\n"
			+ "from challenge where cheassign=?";

	public String todo_update = "update challenge  set followup=? where chid=? ";

	public String todo_getChssign = "select cheassign from challenge where chid=?";

	public String todo_delete = "delete from challenge where chid=?";

	public String todo_challenge = "select challenge from challenge where cheassign=?";

	// For Action Items

	public String get_action_item = "select chid,challenge from challenge where staffe_id =?";

	public String update_action_item = "update challenge set chresolveDate =? where chid=?";

	public String delete_action_item = "delete from challenge  where chid =?";

	public String action_item_due_today = "select count(*) as total From challenge where staffe_id=? && chresolveDate<?";

	// For Goals

	public String getgoals = "select cgoalgroup from corpgoals";

	public String cgoals_getting = "select cgoal_id,cgoals from corpgoals where cgoalgroup=?";

	public String singlegoal = "select cgoal_id,cgoals from corpgoals where cgoal_id= ?";

	public String updatestatusgoal = "update corpgoals set cstatus = ? where cgoal_id =?";

	public String active_strategies_goal = "select  id, category from strategy where goal=? and status != 3";

	public String single_strategy_active_goal = "select id,category from strategy where id=?";

	public String goalsonhold = "select cgoal_id,cgoals from corpgoals where cstatus = 3 && ccreatedby = ?";

	public String goalsunderreview = "select cgoal_id,cgoals from corpgoals where cstatus = 4 && ccreatedby = ?";

	// For Initiatives

	public String priority_one_initiative = "select id,initiative,duedate from strategycategory where ini_level=\"1\" && status != \"3\" && assignoto = ? order by duedate";

	public String priority_two_initiative = "select id,initiative from strategycategory where ini_level= \"2\" && status != \"3\" && assignoto = ? order by duedate";

	public String priority_three_initiative = "select id,initiative  from strategycategory where ini_level=\"3\" && status != \"3\" && assignoto = ? order by duedate";

	public String due_initiatives = "select id,Initiative from strategycategory where  duedate<? && assignoto = ? ";

	public String getting_one_initiative ="select id,initiative from strategycategory where id=?";

	public String change_due_date_initiative = "update strategycategory set duedate =? where ID=?";

	public String change_initiative_status = "update strategycategory set status =? where id =?";

	public String login = "select login_id,login,pswd from sec35_users where login=? and pswd=?";

	
	
	
	public String reassign_initiative = "update strategycategory  set assignoto=? where ID=?";

//	public String past_get_initiative = "select initiative from strategycategory where id= ?";


}
