package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.config.CommonContants;
import com.hubster.dao.TodoDaoImpl;
import com.hubster.model.TodoModel;
import com.hubster.request.Request;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;
import com.hubster.response.Response;



public class TodoUpdateHandler implements RequestHandler<Request, Response> {
	
	private final TodoDaoImpl todoUpdateService = TodoDaoImpl.instance;
	
	private Response getResponse() {
		return new Response(); 
	}

	@Override
	//public LexResponse handleRequest(Request input, Context context) {
	
	public Response handleRequest(Request input,Context context) {
		List<TodoModel> updatelist = todoUpdateService.getUpdatedDate(input);
		Response res = getResponse();
		if (!updatelist.isEmpty()) {

			res.setResCode(CommonContants.SUCCESS_CODE);
			res.setResDesc(CommonContants.SUCCSS_DESC);
			res.setModel(updatelist);
			

		} else {
			//res.setResCode(CommonContants.FAIL_CODE);
			//res.setResDesc(CommonContants.FAIL_DESC);
		}

		//Message message = new Message("PlainText", updatelist.toString());
		//DialogAction dialogueAction = new DialogAction("Close", "Fulfilled", message);

		//return new LexResponse(dialogueAction);
		
		return res;
		
		 
	}

}
