package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.config.CommonContants;
import com.hubster.dao.TodoDaoImpl;
import com.hubster.model.TodoModel;
import com.hubster.request.Request;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;
import com.hubster.response.Response;


public class TodoDeleteHandler implements RequestHandler<Request, Response> {
	
	private final TodoDaoImpl todoDeleteService = TodoDaoImpl.instance;
    
	private Response getResponse() {
		return new Response(); 
	}

	@Override
	public Response handleRequest(Request input, Context context) {
		
		List<TodoModel> deletelist = todoDeleteService.getDeleteTodo(input);
		
		Response response = getResponse();
		if (!deletelist.isEmpty()) {

			response.setResCode(CommonContants.SUCCESS_CODE);
			response.setResDesc(CommonContants.SUCCSS_DESC);
			response.setModel(deletelist);
			

		} else {
			//response.setResCode(CommonContants.FAIL_CODE);
			//response.setResDesc(CommonContants.FAIL_DESC);
		}

		//Message message = new Message("PlainText", deletelist.toString());
		//DialogAction dialogueAction = new DialogAction("Close", "Fulfilled", message);

		//return new LexResponse(dialogueAction);
		return response;
	}

	

}
