package com.hubster.todo;

import java.util.List;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.hubster.dao.TodoDaoImpl;
import com.hubster.model.TodoModel;
import com.hubster.request.Request;
import com.hubster.response.DialogAction;
import com.hubster.response.LexResponse;
import com.hubster.response.Message;
import com.hubster.response.Response;


public class TodoHandler implements RequestHandler<Request, LexResponse> {

	private final TodoDaoImpl todoService = TodoDaoImpl.instance;
	
	private Response getResponse() {
		return new Response(); 
	}

	@Override
	public LexResponse handleRequest(Request input, Context context) {

		List<TodoModel> todolist = todoService.getAllTodos(input);

		Response res = getResponse();
		if (!todolist.isEmpty()) {

			//res.setResCode(CommonContants.SUCCESS_CODE);
			//res.setResDesc(CommonContants.SUCCSS_DESC);
			res.setModel(todolist);
	

		} else {
			//res.setResCode(CommonContants.FAIL_CODE);
			//res.setResDesc(CommonContants.FAIL_DESC);
		}

		//return res;
		Message message = new Message(todolist.toString());
//		DialogAction dialogueAction = new DialogAction(message);

	    return new LexResponse(message);
		//return res;
	}

	
}


